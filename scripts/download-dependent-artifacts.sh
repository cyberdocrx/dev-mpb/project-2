#! /bin/sh

showHelp() {
# `cat << EOF` This means that cat should stop reading when EOF is detected
cat << EOF  
Usage: download-dependent-artifacts.sh 
Download artifacts from a dependent project

If the build was triggered by a depdendent build then use those bits,
otherwise look for bits in the following places:

The branch name that matches this builds branch name of the project in the current namespace
The master branch of the project name in the current namespace
The master branch of the project name in the upstream namespace

Example, currently building project-2 in vanye on the branch fred...

This will look for project-1 artifacts in

vanye/project-1 fred branch
vanye/project-1 master branch
cyberdocrx/dev-mpb/project-1 master branch

in that order and use the first one found.

-h, -help,          --help                  Display help

-t, --trggered-project-name     NAME
-p, --project-name              NAME
-i, --trggered-by-id            ID
-n, --trggered-branch-name      NAME
-j, --job                       NAME
-c, --ci-project-namespace      NAME
-r, --ci-build-ref              REF
-o, --output                    FILENAME
-u, --upstream-project          NAME
EOF
# EOF is found above and hence cat command stops reading. This is equivalent to echo but much neater when printing out.
}

export triggered_project_name=""
export project_name=""
export triggered_by_id=""
export triggered_branch_name=""
export job=""
export ci_project_namespace=""
export ci_build_ref=""
export output="artifacts.zip"
export upstream_project="cyberdocrx%2Fdev-mpb"


# $@ is all command line parameters passed to the script.
# -o is for short options like -v
# -l is for long options with double dash like --version
# the comma separates different long options
# -a is for long options with single dash like -version
options=$(getopt -l "help,triggered-project-name:,project-name:,triggered-by-id:,triggered-branch-name:,job:,ci-project-namespace:,ci-build-ref:,output:" -o "ht:p:i:n:j:c:r:o:" -a -- "$@")

# set --:
# If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters 
# are set to the arguments, even if some of them begin with a ‘-’.
eval set -- "$options"

while true
do
case $1 in
-h|--help) 
    showHelp
    exit 0
    ;;
-t|--trggered-project-name) 
    shift
    export triggered_project_name=$1
    ;;
-p|--project-name)
    shift
    export project_name=$1
    ;;
-i|--trggered-by-id) 
    shift
    export triggered_by_id=$1
    ;;
-n|--trggered-branch-name) 
    shift
    export triggered_branch_name=$1
    ;;
-j|--job)
    shift
    export job=$1
    ;;
-c|--ci-project-namespace) 
    shift
    export ci_project_namespace=$1
    ;;
-r|--ci-build-ref) 
    shift
    export ci_build_ref=$1
    ;;
-o|--output) 
    shift
    export output=$1
    ;;
-u|--upstream-project) 
    shift
    export upstream_project=$1
    ;;
--)
    shift
    break;;
esac
shift
done

if [ -z "$project_name" ]
then
    showHelp
    exit 1
fi

if [ "${triggered_project_name}" = "${project_name}" ]
then
    echo "Downloading artifacts from triggered build"

    curl -sS -L -o $output "https://gitlab.com/api/v4/projects/${triggered_by_id}/jobs/artifacts/${triggered_branch_name}/download?job=${job}&private_token=$GITLAB_API_KEY"
else
    echo "Downloading artifacts from existing builds"

    curl -sS -L -o $output "https://gitlab.com/api/v4/projects/${ci_project_namespace}%2F${project_name}/jobs/artifacts/${ci_build_ref}/download?job=${job}&private_token=$GITLAB_API_KEY" || \
    curl -sS -L -o $output "https://gitlab.com/api/v4/projects/${ci_project_namespace}%2F${project_name}/jobs/artifacts/master/download?job=${job}&private_token=$GITLAB_API_KEY" || \
    curl -sS -L -o $output "https://gitlab.com/api/v4/projects/${upstream_project}%2F${project_name}/jobs/artifacts/master/download?job=${job}&private_token=$GITLAB_API_KEY"
fi

ls -l

